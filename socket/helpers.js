export function changeArray (oldArray, newElem, idx) {
    return [
      ...oldArray.slice(0, idx),
      newElem,
      ...oldArray.slice(idx + 1)
    ]
}


export function getRandomNumber(min, max) {
  const rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}