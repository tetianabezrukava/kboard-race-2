export function loginErrorListener() {
  alert('There is an active user with such name. \n Please chose another name.');
  sessionStorage.clear();
  window.location.replace("/login");
}

export function addRoomErrorListener() {
  alert('There is a room with such name. \n Please, give your room another name.');
}