import {
  createRoom,
  createGamePlayer, createReadyBtn
} from '../helpers.mjs';
import { socket , username } from '../game.mjs';
import { switchToRoomsPage } from '../helpers.mjs';

export function loadRoomPageListener({ username, rooms }) {
  const gritting = document.querySelector('h1');
  gritting.innerHTML = `Welcome to KBoard-Race, ${username}!`;
  updateRoomsListener(rooms);
}

export function loadGamePageListener(data) {
  const { roomName, users, currentTextLength } = data;
  const gameTitle = document.querySelector('#game-parameters h2');
  gameTitle.innerHTML = roomName;
  const backToRoomsBtn = document.getElementById('quit-room-btn');
  backToRoomsBtn.addEventListener('click', () => onBackToRoomsClick(roomName), {once: true});
  const gameField = document.getElementById('game-field');
  gameField.innerHTML = '';
  const readyBnt = createReadyBtn();
  gameField.appendChild(readyBnt);
  updateProgressBarListener({users, currentTextLength})
}

export function updateProgressBarListener({ users, currentTextLength}) {
  const playersList = document.getElementById('game-players');
  playersList.innerHTML = '';
  const players = Object.keys(users);
  players.map((player) => {
    const data = {
      currentTextLength,
      name: player,
      status: users[player].status,
      progress: users[player].progress,
      isCurrentUser: player === username
    }
    
    const listItem = createGamePlayer(data);
    playersList.appendChild(listItem)
  })
}

export function updateRoomsListener(rooms) {
  const roomsContainer = document.querySelector('.rooms-container');
  roomsContainer.innerHTML = '';
  rooms.map(({ roomName, usersNumber, isAvailable }) => {
    if (isAvailable) {
      const roomCard = createRoom(roomName, usersNumber);
      roomsContainer.appendChild(roomCard);
    }
  });
}

function onBackToRoomsClick(roomName) {
  socket.emit("LEAVE_ROOM", roomName);
  switchToRoomsPage();
}
