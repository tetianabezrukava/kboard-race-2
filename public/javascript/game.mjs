import {
  loginErrorListener,
  addRoomErrorListener
} from './listeners/errorListeners.mjs';
import {
  updateRoomsListener,
  loadRoomPageListener,
  loadGamePageListener,
  updateProgressBarListener
} from './listeners/updateListeners.mjs';
import { prepareForGameListener, logGameResultListener } from './listeners/gameListeners.mjs';
import { switchToGamePage } from './helpers.mjs';

export const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", { query: { username } });

const addRoomBtn = document.getElementById('add-room-btn');
addRoomBtn.addEventListener('click', onAddRoomClick);


socket.on("LOGIN_ERROR", loginErrorListener);
socket.on("ADD_ROOM_ERROR", addRoomErrorListener);
socket.on("LOAD_ROOM_PAGE", (data) => loadRoomPageListener(data));
socket.on("LOAD_GAME_PAGE", (data) => loadGamePageListener(data));
socket.on("UPDATE_ROOMS", (data) => updateRoomsListener(data));
socket.on("UPDATE_PROGRESS_BAR", (data) => updateProgressBarListener(data));
socket.on("PREPARE_FOR_GAME", (data) => prepareForGameListener(data));
socket.on("LOG_RESULT", (data) => logGameResultListener(data));
socket.on("ROOM_WAS_ADDED", (data) => {
  socket.emit("JOIN_ROOM", data);
  socket.emit("GET_DATA_FOR_GAME_PAGE", data);
  switchToGamePage();
});

function onAddRoomClick() {
  const roomName = prompt('Write Room Name');
  if (roomName && roomName.trim()) {
    socket.emit("ADD_ROOM", roomName);
  }
}