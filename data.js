export const texts = [
  "JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification.",
  "Alongside HTML and CSS, JavaScript is one of the core technologies of the World Wide Web.",
  "The Mosaic web browser was released in 1993. As the first browser with a graphical user interface accessible to non-technical people, it played a prominent role in the rapid growth of the nascent World Wide Web. ",
  "As a multi-paradigm language, JavaScript supports event-driven, functional, and imperative programming styles.",
  "The ECMAScript standard does not include any input/output (I/O), such as networking, storage, or graphics facilities.",
  "In 2005, Mozilla joined ECMA International, and work started on the ECMAScript for XML (E4X) standard. This led to Mozilla working jointly with Macromedia (later acquired by Adobe Systems), who were implementing E4X in their ActionScript 3 language, which was based on an ECMAScript 4 draft.",
  "In 2005, Jesse James Garrett released a white paper in which he coined the term Ajax and described a set of technologies, of which JavaScript was the backbone, to create web applications where data can be loaded in the background, avoiding the need for full page reloads.",
];

export default { texts };
